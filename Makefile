ASCIIDOC := asciidoctor

build: $(wildcard *.asciidoc)
	rm -rf build
	mkdir build
	$(ASCIIDOC) --backend html5 --out build/packaging.html packaging.asciidoc
	$(ASCIIDOC) --backend html5 --out build/workflow-changes.html workflow-changes.asciidoc
	$(ASCIIDOC) --backend html5 --out build/ci.html ci.asciidoc
	$(ASCIIDOC) --backend html5 --out build/index.html index.asciidoc
